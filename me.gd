extends CharacterBody3D

var target_velocity = Vector3.ZERO
var camera_anglev=0
var rota=0
# Called when the node enters the scene tree for the first time.
func _ready():
	print(rotation)
	pass # Replace with function body.
@export var speed = -7
# The downward acceleration when in the air, in meters per second squared.
@export var fall_acceleration = 75
var global_direction = Vector3(0,0,1)
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _physics_process(delta):
	if Input.is_action_pressed("move_right"):
		rotation.y -= 0.05
	if Input.is_action_pressed("move_left"):
		rotation.y += 0.05
	if Input.is_action_pressed("move_for"):
		move(delta,1)
	if Input.is_action_pressed("move_back"):
		move(delta,-1)
	if Input.is_action_pressed("look_up"):
		$Camera3D.rotate_x(0.15);
	if Input.is_action_pressed("look_down"):
		$Camera3D.rotate_x(-0.15);
			
func getLocalDirection() -> Vector3:
	return global_direction.rotated(Vector3(0,1,0), rotation.y)
	
func move(delta, direct):
	# Ground Velocity
	var local_direction = getLocalDirection()
	# Vertical Velocity
	if not is_on_floor(): # If in the air, fall towards the floor. Literally gravity
		local_direction.y = (fall_acceleration * delta)
	velocity = local_direction * speed *direct
	move_and_slide()
