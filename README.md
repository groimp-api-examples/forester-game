## GroIMP forester game

A Godot4 based game that uses [GroIMP](https://grogra.de) as a growth engine through the [GroLink API](https://wiki.grogra.de/doku.php?id=user-guide:additional_interfaces:api). It requires GroIMP and GroLink.
This is only designed as an simple example usecase of the GroIMP API. 


![](screenshots/game.png)


### Installation

The game is [released](https://gitlab.com/groimp-api-examples/forester-game/-/releases) for windows and linux as zip files including each an executable, beside this the repository can be opend with the Godot editor and launced from there.
The Game needs GroIMP including the [API plugin](https://gitlab.com/grogra/groimp-plugins/api) and the [Forest-Plugin](https://gitlab.com/groimp-api-examples/forester-plugin) installed, both can be found in the [GroIMP plugin manager](https://wiki.grogra.de/doku.php?id=user-guide:pluginmanager).


### Runing

After [starting the API](https://wiki.grogra.de/doku.php?id=tutorials:startup-api) and the game, it should be possible to connect to the API from the game by setting the right address (provided by the cmd panel of the api) and clicking on connect. After clicking the available simulation should appear in the list box and can be selected and play with or without trees.




### Used resources:

- https://github.com/Ezcha/gd-obj
- https://opengameart.org/content/game-wood-panel
- https://opengameart.org/content/blended-textures-of-dirt-and-grass



The project is described in more detail in [GroLink: implementing and testing a general application programming interface for the plant-modelling platform GroIMP](https://wkurth.grogra.de/oberlaender_msc.pdf) section 4.3.
