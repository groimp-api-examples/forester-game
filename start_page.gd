extends CanvasLayer
var url=""
var json;

func _ready():
	if(Global.getURL()!=null):
		$Panel/URL.text=Global.getURL()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_connect_pressed():
	url = $Panel/URL.text
	$loadExamples.request(""+url+"/api/app/ui/commands/app/listExamples?tags=forester")
	Global.setURL(url)

func _on_con_request_completed(result, response_code, headers, body):
	if(response_code!=200):
		print("url not valid")
	else:
		json = JSON.parse_string(body.get_string_from_utf8())
		$Panel/gameList.clear()
		for d in json['data']:
			$Panel/gameList.add_item(d['title'])

func _on_game_list_item_selected(index):
	$Panel/description.text = json['data'][index]['description']



func _on_start_pressed():
	if(json!=null && $Panel/gameList.is_anything_selected()):
		var id =$Panel/gameList.get_selected_items()[0]
		var exId = (json['data'][id]['id'])
		$openExample.request(""+url+"/api/app/ui/commands/app/loadExample?name="+exId)


func _on_open_example_request_completed(result, response_code, headers, body):
	if(response_code!=200):
		print("name not valid")
	else:
		var j = JSON.parse_string(body.get_string_from_utf8())
		var id = j['id']
		Global.setID(id)
		$getTreeRules.request(""+url+"/api/wb/"+id+"/ui/commands/runRGGFunction?name=getTreeRules")
		



#		get_tree().change_scene_to_file("res://Game.tscn")


func _on_get_tree_rules_request_completed(result, response_code, headers, body):
	if(response_code!=200):
		print("model not valid")
	else:
		var j = JSON.parse_string(body.get_string_from_utf8())
		Global.setcTree(j['console'][0]);
		Global.setdTree(j['console'][1]);
		if($Panel/grown_trees.is_pressed()):
			$setTrees.request(""+url+"/api/wb/"+Global.getID()+"/ui/commands/runRGGFunction?name=initialize")
		else:
			get_tree().change_scene_to_file("res://Game.tscn")
		


func _on_set_trees_request_completed(result, response_code, headers, body):
		get_tree().change_scene_to_file("res://Game.tscn")
	
