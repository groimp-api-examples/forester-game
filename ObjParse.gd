extends Object
class_name ObjParse

const debug: bool = false

# gd-obj
#
# Created on 7/11/2018
#
# Originally made by Ezcha
# Contributors: deakcor, kb173, jeffgamedev
#
# https://ezcha.net
# https://github.com/Ezcha/gd-obj
#
# MIT License
# https://github.com/Ezcha/gd-obj/blob/master/LICENSE

# Create mesh from obj and mtl paths

# Create mesh from obj, materials. Materials should be { "matname": data }
static func load_obj_from_buffer(obj_data: String, materials: Dictionary) -> Mesh:
	return _create_obj(obj_data,materials)

static func _create_obj(obj: String, mats: Dictionary) -> Mesh:
	# Setup
	var mesh: ArrayMesh = ArrayMesh.new()
	var vertices: PackedVector3Array = PackedVector3Array()
	var colors: PackedColorArray = PackedColorArray()
	var normals: PackedVector3Array = PackedVector3Array()
	var uvs: PackedVector2Array = PackedVector2Array()
	var faces: Dictionary = {}

	var mat_name: String = "default"
	var count_mtl: int =0
	
	# Parse
	var lines: PackedStringArray = obj.split("\n", false)
	for line in lines:
		var parts: PackedStringArray = line.split(" ", false)
		match parts[0]:
			"#":
				# Comment
				pass
			"v":
				# Vertice
				var n_v: Vector3 = Vector3(parts[1].to_float(), parts[2].to_float(), parts[3].to_float())
				vertices.append(n_v)
				var n_c: Color = Color.BLACK
				if(parts.size()==7):
					n_c  = Color(parts[4].to_float(), parts[5].to_float(), parts[6].to_float())
				colors.append(n_c)
		#	"vn":
				# Normal
		#		var n_vn: Vector3 = Vector3(parts[1].to_float(), parts[2].to_float(), parts[3].to_float())
		#		normals.append(n_vn)
		#	"vt":
				# UV
		#		var n_uv: Vector2 = Vector2(parts[1].to_float(), 1 - parts[2].to_float())
		#		uvs.append(n_uv)
		#	"usemtl":
				# Material group
		#		count_mtl += 1
		#		mat_name = parts[1].strip_edges()
		#		if (!faces.has(mat_name)):
		#			var mats_keys: Array = mats.keys()
			#		if (!mats.has(mat_name)):
			#			if (mats_keys.size() > count_mtl):
			#				mat_name = mats_keys[count_mtl]
			#		faces[mat_name] = []
			"f":
				if (!faces.has(mat_name)):
					var mats_keys: Array = mats.keys()
					if (mats_keys.size() > count_mtl):
						mat_name = mats_keys[count_mtl]
					faces[mat_name] = []
				# Face
				if (parts.size() == 4):
					# Tri
					var face: Dictionary = { "v": [], "vt": [], "vn": [] }
					for map in parts:
						var vertices_index: PackedStringArray = map.split("/")
						if (vertices_index[0] != "f"):
							face["v"].append(vertices_index[0].to_int() - 1)
						#	face["vt"].append(vertices_index[1].to_int() - 1)
						#	if (vertices_index.size() > 2):
						#		face["vn"].append(vertices_index[2].to_int() - 1)
					if (faces.has(mat_name)):
						faces[mat_name].append(face)
				elif (parts.size() > 4):
					# Triangulate
					var points: Array[Array] = []
					for map in parts:
						var vertices_index: PackedStringArray = map.split("/")
						if (vertices_index[0] != "f"):
							var point: Array[int] = []
							point.append(vertices_index[0].to_int() - 1)
							point.append(vertices_index[1].to_int() - 1)
							if (vertices_index.size() > 2):
								point.append(vertices_index[2].to_int()-1)
							points.append(point)
					for i in (points.size()):
						if (i != 0):
							var face = { "v":[], "vt":[], "vn":[] }
							var point0: Array[int] = points[0]
							var point1: Array[int] = points[i]
							var point2: Array[int] = points[i-1]
							face["v"].append(point0[0])
							face["v"].append(point2[0])
							face["v"].append(point1[0])
						#	face["vt"].append(point0[1])
						#	face["vt"].append(point2[1])
						#	face["vt"].append(point1[1])
						#	if (point0.size() > 2):
						#		face["vn"].append(point0[2])
						#	if (point2.size() > 2):
						#		face["vn"].append(point2[2])
						#	if (point1.size() > 2):
						#		face["vn"].append(point1[2])
							faces[mat_name].append(face)
	
	# Make tri
	for matgroup in faces.keys():
		if debug:
			prints("Creating surface for matgroup", matgroup, "with", str(faces[matgroup].size()), "faces")
		
		# Mesh Assembler
		var st: SurfaceTool = SurfaceTool.new()
		st.begin(Mesh.PRIMITIVE_TRIANGLES)
		if (!mats.has(matgroup)):
			mats[matgroup] = StandardMaterial3D.new()
		st.set_material(mats[matgroup])
		for face in faces[matgroup]:
			if (face["v"].size() == 3):
				# Vertices
				var fan_v: PackedVector3Array = PackedVector3Array()
				var fan_c: PackedColorArray = PackedColorArray()
				fan_v.append(vertices[face["v"][0]])
				fan_v.append(vertices[face["v"][2]])
				fan_v.append(vertices[face["v"][1]])
				
				fan_c.append(colors[face['v'][0]])
				fan_c.append(colors[face['v'][1]])
				fan_c.append(colors[face['v'][2]])

				
				
				# Normals
				var fan_vn: PackedVector3Array = PackedVector3Array()
				if (face["vn"].size() > 0):
					fan_vn.append(normals[face["vn"][0]])
					fan_vn.append(normals[face["vn"][2]])
					fan_vn.append(normals[face["vn"][1]])
				# Textures
				var fan_vt: PackedVector2Array = PackedVector2Array()
				if (face["vt"].size() > 0):
					for k in [0,2,1]:
						var f = face["vt"][k]
						if (f > -1):
							var uv = uvs[f]
							fan_vt.append(uv)
				st.add_triangle_fan(fan_v, fan_vt,fan_c, PackedVector2Array(), fan_vn, [])
		mesh = st.commit(mesh)
	
	for k in mesh.get_surface_count():
		var mat: Material = mesh.surface_get_material(k)
		mat_name = ""
		for m in mats:
			if (mats[m] == mat):
				mat_name = m
		mesh.surface_set_name(k, mat_name)
	
	# Finish
	return mesh
