extends WorldEnvironment
#https://opengameart.org/content/blended-textures-of-dirt-and-grass

var id
var ray_length = 1000
var mode=0
var cash=10
# Called when the node enters the scene tree for the first time.
func _ready():
	id =Global.getID()
	update()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func raycast_from_mouse(m_pos, collision_mask):
	var ray_start = $me/Camera3D.project_ray_origin(m_pos)
	var ray_end = ray_start + $me/Camera3D.project_ray_normal(m_pos) * ray_length
	var world3d : World3D =  $Floor/MeshInstance3D.get_world_3d()
	var space_state = world3d.direct_space_state
	
	if space_state == null:
		return
	
	var query = PhysicsRayQueryParameters3D.create(ray_start, ray_end, collision_mask)
	query.collide_with_areas = true
	
	return space_state.intersect_ray(query)


func _unhandled_input(event):
	if Input.is_action_pressed("click"):
		var position2D = get_viewport().get_mouse_position()
		if(position2D.y >get_viewport().get_visible_rect().size.y/2):
			var result= (raycast_from_mouse(position2D,1))
			print(result.position)
			var x = result.position.x
			var z = result.position.z
			if(mode==0):
				cash-=1
				$CanvasLayer/TextureRect/Cash.text = "Cash: "+str(cash)
				$http/plant.request(Global.getURL()+"/api/wb/"+id+"/ui/commands/runXLQuery?xl=[Floor==>Floor[Translate("+str(x)+","+str(z)+",0)"+Global.getcTree()+"];]")
			if(mode==1):
				cash-=1
				$CanvasLayer/TextureRect/Cash.text = "Cash: "+str(cash)
				$http/plant.request(Global.getURL()+"/api/wb/"+id+"/ui/commands/runXLQuery?xl=[Floor==>Floor[Translate("+str(x)+","+str(z)+",0)"+Global.getdTree()+"];]")
			if(mode==2):
				var headers = ["Content-Type: application/json"]
				$http/cut.request(Global.getURL()+"/api/wb/"+id+"/ui/commands/runXLQuery",headers, HTTPClient.METHOD_POST,"[t:Model.Tree,(location(t).x > "+str(x-0.15)+" && location(t).x < "+str(x+0.15)+" && location(t).y > "+str(z-0.15)+" && location(t).y < "+str(z+0.5)+")::>{println(t.getVolume());[t==>>;]}]")
			if(mode==3):
				var headers = ["Content-Type: application/json"]
				$http/info.request(Global.getURL()+"/api/wb/"+id+"/ui/commands/runXLQuery",headers, HTTPClient.METHOD_POST,"[t:Model.Tree,(location(t).x > "+str(x-0.15)+" && location(t).x < "+str(x+0.15)+" && location(t).y > "+str(z-0.15)+" && location(t).y < "+str(z+0.5)+")::>{t.getInfo();}]")

	if event is InputEventKey:
		if event.pressed and event.keycode == KEY_SPACE and id!=null:
			$http/grow.request(Global.getURL()+"/api/wb/"+id+"/ui/commands/runRGGFunction?name=run")



func _notification(what):
	if what == NOTIFICATION_WM_CLOSE_REQUEST:
		$http/close.request(Global.getURL()+"/api/wb/"+id+"/ui/commands/close")
		get_tree().quit() # default behavior


func update():
	$http/getOBJ.request(Global.getURL()+"/api/wb/"+id+"/ui/commands/export3d?extension=obj")


func _on_get_obj_request_completed(result, response_code, headers, body):
	var newMesh = ObjParse.load_obj_from_buffer(body.get_string_from_utf8(),{})
	var mar =  StandardMaterial3D.new()
	mar.vertex_color_use_as_albedo = true
	mar.SHADING_MODE_PER_VERTEX= true
	newMesh.surface_set_material(0,mar)
	$base.set_mesh(newMesh)


func _on_cut_request_completed(result, response_code, headers, body):
	var results = JSON.parse_string(body.get_string_from_utf8())['console']
	if len(results)>0:
		var vol =results[0].to_float()
		#vol = vol*25
		vol = snapped(vol,0.01)
		cash+=vol
		$CanvasLayer/TextureRect/Cash.text = "Cash: "+str(cash)
	update()


func _on_grow_request_completed(result, response_code, headers, body):
	update()

func _on_plant_request_completed(result, response_code, headers, body):
	update()


func _on_cut_pressed():
	mode=2
	$CanvasLayer/buttonBar/looking.texture_normal=load("res://img/looking.png")
	$CanvasLayer/buttonBar/cut.texture_normal=load("res://img/axt_s.png")
	$CanvasLayer/buttonBar/tree_c.texture_normal =load("res://img/tree_c.png")
	$CanvasLayer/buttonBar/tree_d.texture_normal =load("res://img/tree_d.png")
	
	


func _on_tree_d_pressed():
	mode=1
	$CanvasLayer/buttonBar/looking.texture_normal=load("res://img/looking.png")
	$CanvasLayer/buttonBar/cut.texture_normal=load("res://img/axt.png")
	$CanvasLayer/buttonBar/tree_c.texture_normal =load("res://img/tree_c.png")
	$CanvasLayer/buttonBar/tree_d.texture_normal =load("res://img/tree_d_s.png")

func _on_tree_c_pressed():
	mode=0
	$CanvasLayer/buttonBar/looking.texture_normal=load("res://img/looking.png")
	$CanvasLayer/buttonBar/cut.texture_normal=load("res://img/axt.png")
	$CanvasLayer/buttonBar/tree_c.texture_normal =load("res://img/tree_c_s.png")
	$CanvasLayer/buttonBar/tree_d.texture_normal =load("res://img/tree_d.png")


func _on_time_pressed():
	$http/grow.request(Global.getURL()+"/api/wb/"+id+"/ui/commands/runRGGFunction?name=run")


func _on_texture_button_pressed():
	$http/close.request(Global.getURL()+"/api/wb/"+id+"/ui/commands/close")
	get_tree().change_scene_to_file("res://start_page.tscn")


func _on_help_pressed():
	$CanvasLayer/Info/content.text="Help:
- Rotate left and right: a,d
- Move forward and back: w,s
- look up and down: q,e"
	$CanvasLayer/Info.show()


func _on_info_close_pressed():
	$CanvasLayer/Info.hide()


func _on_looking_pressed():
	mode=3
	$CanvasLayer/buttonBar/looking.texture_normal=load("res://img/looking_s.png")
	$CanvasLayer/buttonBar/cut.texture_normal=load("res://img/axt.png")
	$CanvasLayer/buttonBar/tree_c.texture_normal =load("res://img/tree_c.png")
	$CanvasLayer/buttonBar/tree_d.texture_normal =load("res://img/tree_d.png")


func _on_info_request_completed(result, response_code, headers, body):
	var results = JSON.parse_string(body.get_string_from_utf8())['console']
	if len(results)>0:
		$CanvasLayer/Info/content.text=results[0]
		$CanvasLayer/Info.show()
